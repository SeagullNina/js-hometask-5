'use strict';
/*
    В этом задании нельзя менять разметку, только писать код в этом файле.
 */

/**
*   1. Удали со страницы элемент с id "deleteMe"
**/

function removeBlock() {
    const elem = document.getElementById('deleteMe');
    elem.remove();
}

/**
 *  2. Сделай так, чтобы во всех элементах с классом wrapper остался только один параграф,
 *  в котором будет сумма чисел из всех параграфов.
 *
 *  Например, такой элемент:
 *
 *  <div class="wrapper"><p>5</p><p>15</p><p>25</p><p>35</p></div>
 *
 *  должен стать таким
 *
 *  <div class="wrapper"><p>80</p></div>
 */

function calcParagraphs() {
    const wrappers = document.getElementsByClassName('wrapper');
    for (let wrapper of wrappers) {
        const array = wrapper.querySelectorAll('p');
        let sum = 0;
        for (let i = 0; i < array.length; i++) {
            sum += Number(array[i].textContent);
            array[i].remove();
        }
        const numbers = document.createElement('p');
        numbers.innerText = `${sum}`;
        wrapper.appendChild(numbers);
    }
}

/**
 *  3. Измени value и type у input c id "changeMe"
 *
 */

function changeInput() {
    const elem = document.getElementById('changeMe');
    elem.value = 'element';
    elem.type = 'checkbox';
}

/**
 *  4. Используя функции appendChild и insertBefore дополни список с id "changeChild"
 *  чтобы получилась последовательность <li>0</li><li>1</li><li>2</li><li>3</li>
 *
 */

function appendList() {
    const list = document.getElementById('changeChild');
    const firstli = document.createElement('li');
    firstli.innerText = '1';
    list.insertBefore(firstli, list.querySelectorAll('li')[1]);
    const thirdli = document.createElement('li');
    thirdli.innerText = '3';
    list.appendChild(thirdli);
}

/**
 *  5. Поменяй цвет всем div с class "item".
 *  Если у блока class "red", то замени его на "blue" и наоборот для блоков с class "blue"
 */

function changeColors() {
    const elem = document.getElementsByClassName('item');
    for(let i = 0; i < elem.length; i++) {
        if (elem.item(i).classList.contains('red')) {
            elem.item(i).style.backgroundColor = 'blue'; }
            else
            if (elem.item(i).classList.contains('blue')) {
                elem.item(i).style.backgroundColor = 'red'; }
    }
}

removeBlock();
calcParagraphs();
changeInput();
appendList();
changeColors();
